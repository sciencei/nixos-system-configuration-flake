{ config, lib, pkgs, nur, neovim-nightly-overlay, xmonad-config, zsh-nix-shell, theme }:
let
  homeDirectory = /home/sciencei;
  homePathStr = toString homeDirectory;
  standalones   = ./standalones;
  systemScripts = ./system-scripts;
  emptyDir      = ./.empty;
  polybar = import (standalones + /polybar.nix) {inherit pkgs theme;};
  # TODO: make theme reliant on system theme
  neovim = import (standalones + /vim.nix) {inherit pkgs;};
  kittyConf = import (standalones + /kitty.nix) {inherit pkgs theme;};
in
{
  home.stateVersion = "20.03";

  # basic misc
  programs.home-manager.enable = true;
  fonts.fontconfig.enable = true;
  services.gpg-agent = {
    enable = true;
    defaultCacheTtl = 1800;
    enableSshSupport = true;
  };
  # needed for network manager to be able to extract secrets
  services.network-manager-applet.enable = true;

  # create any files and folders
  home.file =
    { scripts = {
        source = systemScripts;
        recursive = true;
      };
      ".config/kitty/kitty.conf" = {
        text = kittyConf;
      };
      ".config/ranger/rc.conf" = {
        text = ''
          set preview_images true
          set preview_images_method kitty
        '';
      };
    } //
    # create a bunch of empty (except a .gitignore) directories
    lib.flip lib.genAttrs (name: { source = emptyDir; recursive = true; }) [
      ".history"
      ".log"
      "builds"
      "doc"
      "music"
      "pic"
      "shots/window"
      "shots/cropped"
      "shots/full"
      "torrents"
      "vid"
    ];

  # basic 'desktop environment'
  services.polybar = polybar;
  xsession = {
    enable = true;
    scriptPath = ".hm-xsession";
    windowManager.xmonad = {
      enable = true;
      enableContribAndExtras = true;
      config = xmonad-config.config;
      extraPackages = xmonad-config.deps;
    };
  };

  # programs with no special configuration needed
  home.packages = with pkgs; [
    alsaTools
    asciinema
    bitwarden
    bind
    blueman
    cbatticon
    colorpicker
    deluge
    discord-canary
    dmenu
    dzen2
    ephemeralpg
    expect
    feh
    ffmpeg
    firefox
    fontconfig
    fzf
    gcc
    gimp
    gnome3.gucharmap
    gnome3.seahorse
    google-chrome
    haskell-language-server
    htop
    jq
    keepassx2
    libnotify
    libreoffice
    lsof
    maim
    networkmanagerapplet
    nix-index
    nix-prefetch-git
    nmap
    openjdk8
    parted
    pasystray
    pavucontrol
    pick-colour-picker
    postgresql_12
    psmisc
    ranger
    redshift
    ripgrep
    rsync
    signal-desktop
    # a font, technically
    siji
    slack
    sxiv
    sysfsutils
    twmn
    # displays images
    ueberzug
    unrar
    unzip
    xdotool
    xmlstarlet
    xorg.xev
    xorg.xfd
    xorg.xfontsel
    xorg.xlsfonts
    yq
  ];

  # programs with special configuration
  programs.neovim = neovim;

  # screenshots
  services.flameshot = {
    enable = true;
    settings = {
      General = {
        filenamePattern = "%F-%H-%M-%S";
        disabledTrayIcon = true;
        savePath = "${homePathStr}/shots/cropped";
        savePathFixed = true;
        showStartupLaunchMessage = false;
        showHelp = false;
        saveAsFileExtension = "png";
        drawThickness = 1;
      };
      Shortcuts = {
        TYPE_IMAGEUPLOADER = "Ctrl+U";
        TYPE_SAVE = "Return";
      };
    };
  };

  # TODO: remember what this is for
  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    "nix-direnv".enable = true;
  };

  programs.git = {
    enable = true;
    userName = "Cullin Poresky";
    userEmail = "cporeskydev@gmail.com";
  };

  programs.firefox = {
    enable = true;
    package = pkgs.firefox;
    extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      multi-account-containers
      https-everywhere
      ublock-origin
      bitwarden
    ];
    profiles.default = {
      id = 0;
      settings = {
        # download to dl dir
        "browser.download.dir" = "${homePathStr}/dl";
        "browser.download.folderList" = 2;
        # fix screen tearing
        "mozilla.widget.use-argb-visuals" = true;
      };
    };
  };
  
  # tells me what package to install/run in a shell when I run a command
  # that's not found
  programs.command-not-found.enable = true;

  # xresources; currently just sxiv configuration
  xresources.properties = {
    "Sxiv.background" = "${theme.colors.background}";
    "Sxiv.foreground" = "${theme.colors.foreground}";
    "Sxiv.font" = "${theme.font.name}-${toString theme.font.size}";
  };

  programs.mpv = {
    enable = true;
    config = {
      hwdec   = "auto";
      vo      = "gpu";
    };
  };

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autocd = true;
    defaultKeymap = "viins";
    # keeps this out of my 
    dotDir = ".config/zsh";
    history = {
      path = ".history/zsh";
      expireDuplicatesFirst = true;
    };
    shellAliases = {
      sudo = "sudo ";
    };
    sessionVariables = {
      _JAVA_AWT_WM_NONREPARENTING = 1;
      PROMPT = "%F{${theme.colors.accent}}X> %f";
      EDITOR = "nvim";
      PATH = "${homePathStr}/scripts:$PATH";
      FZF_DEFAULT_COMMAND="${pkgs.ripgrep}/bin/rg --files";
    };
    plugins = [
      {
        name = "zsh-nix-shell";
        src = zsh-nix-shell;
      }
    ];
    initExtra = ''
      bindkey -r '^I'
      bindkey '^N' expand-or-complete
    '';
  };
}
