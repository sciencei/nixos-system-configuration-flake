{ pkgs, theme }:
let
  vpnStatus = pkgs.writeScript "vpnStatus" ''
    #!/bin/sh
    if [[ $(${pkgs.mullvad-vpn}/bin/mullvad status | ${pkgs.gnugrep}/bin/grep 'Connected to') ]]
    then printf '\ue1f7\n'
    else printf '\ue0b2\n'
    fi
  '';
  dicschneearyStatus = pkgs.writeScript "dicschneearyStatus" ''
    #/bin/sh
    val=$(${pkgs.openssh}/bin/ssh dicschneeary "journalctl -u dicschneeary -S '2 minutes ago' | grep dicschneeary-exe")
    if [ -n "$val" ]
    then printf '\ue1e9\n'
    else printf '\ue0b3\n'
    fi
   '';
in
{
  package = pkgs.polybarFull;
  script = "polybar default &";
  enable = true;
  config = {
    "bar/default" = {
      monitor = "";
      monitor-strict = true;
      modules-left = ["xmonad-ws"];
      modules-right = [
        "cpu"
        "memory"
        "pulseaudio"
        "battery"
        "wifi"
        "vpn"
        "dicschneeary"
        "date"
      ];
      # we always use siji, because we need it for, well, sigils
      font-0 = "Siji:size=${(toString theme.font.size)};2";
      font-1 = "${theme.font.name}:size=${(toString theme.font.size)};3";
      height = theme.font.size + 11;
      border-bottom-size = 1;
      border-color = "${theme.colors.accent}";
      locale = "en_US.UTF-8";
      wm-name = "polybar-default";
      module-margin-left = 1;
    };
    "settings" = {
      format-foreground = "${theme.colors.foreground}";
      format-background = "${theme.colors.accent2}";
      format-padding = 1;
    };
    "module/date" = {
      type = "internal/date";
      date = "%a %b %d %Y %I:%M:%S %p";
    };
    "module/xmonad-ws" = {
      type = "custom/script";
      exec = "${pkgs.coreutils}/bin/tail -F -n 1 /tmp/.xmonad-bar-log";
      exec-if = "[ -e /tmp/.xmonad-bar-log ]";
      tail = true;
      format-padding = 0;
    };
    "module/vpn" = {
      type = "custom/script";
      exec = "${vpnStatus}";
    };
    "module/dicschneeary" = {
      type = "custom/script";
      exec = "${dicschneearyStatus}";
      interval = 600;
    };
    "module/battery" = {
      type = "internal/battery";
      battery = "BAT0";
      adapter = "AC";
      format-charging = " <label-charging>";
      time-format = "%_Hh";
      format-discharging = "<ramp-capacity> <label-discharging>";
      format-full = "<label-full>";
      label-charging = "%percentage%% %time%";
      label-discharging = "%percentage%% %time%";
      label-full = " 100%";
      ramp-capacity-0 = "";
      ramp-capacity-1 = "";
      ramp-capacity-2 = "";
    };
    "module/pulseaudio" = {
      type = "internal/pulseaudio";
      format-volume = "<label-volume> <bar-volume>";
      format-muted = "<label-muted> <bar-volume>";
      bar-volume-width = 6;
      label-muted = "";
      label-volume = "";
      bar-volume-indicator = "|";
      bar-volume-fill = "─";
      bar-volume-empty = "─";
    };
    "module/cpu" = {
      type = "internal/cpu";
      interval = 2;
      format-prefix = " ";
      label = "%percentage%%";
    };
    "module/memory" = {
      type = "internal/memory";
      interval = 2;
      format-prefix = " ";
      label = "%percentage_used%%";
    };
    "module/wifi" = {
      type = "internal/network";
      interface = "wlp0s20f3";
      interval = 3;
      format-connected = "<ramp-signal> <label-connected>";
      label-disconnected = " Disconnected";
      label-connected = "%downspeed% %upspeed%";
      ramp-signal-0 = "";
      ramp-signal-1 = "";
      ramp-signal-2 = "";
    };
  };
}
