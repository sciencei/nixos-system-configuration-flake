{ pkgs }:
let
  color-scheme = import ./weiss-vim-color.nix { inherit pkgs; };
in
{
  package = pkgs.neovim-nightly;
  enable = true;
  vimAlias = true;
  plugins = with pkgs.vimPlugins; [
    # fuzzy find
    fzf-vim
    # git
    vim-fugitive
    # color code highlighting
    vim-hexokinase
    # lsp
    nvim-lspconfig
    # go to line
    vim-fetch
    # color scheme
    color-scheme
    # treesitter: syntax highlighting
    nvim-treesitter
    # completion
    nvim-cmp
    # via lsp
    cmp-nvim-lsp
    # via buffer
    cmp-buffer
    # via path
    cmp-path
    # haskell syntax highlighting and indent
    haskell-vim
  ];
  extraConfig = ''
    " colorscheme
    set termguicolors
    set background=dark
    color weiss

    " syntax
    syntax enable

    " echodoc
    let g:echodoc_enable_at_startup = 1
    set cmdheight=2

    " indent
    set shiftwidth=2
    set tabstop=2
    set expandtab
    filetype plugin indent on

    " search
    set incsearch
    set hlsearch
    set ic
    set smartcase

    " keybindings
    noremap <a-l> :bn <CR>
    noremap <a-h> :bp <CR>
    nnoremap j gj
    nnoremap k gk
    nnoremap gj j
    nnoremap gk k
    noremap ff :Files <CR>
    noremap fb :Buffer <CR>
    noremap ft :Tags <CR>
    noremap fl :Lines <CR>
    noremap fe :Ghcid <CR>

    " misc
    set undofile
    set ruler
    set magic
    set guicursor=
    set encoding=utf-8
    set showcmd
    set backspace=indent,eol,start
    set number
    autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif 
    inoremap <expr> <CR> pumvisible() ? "\<C-y>" . "\<CR>" : "\<CR>"
    let mapleader = ","
    set hidden
    autocmd BufNewFile,BufReadPost *.md set filetype=markdown

    " fzf/ghcid
    function! s:cleanopen(f)
      let clean = system("cut -d: -f3,4", a:f)
      execute "edit " . clean
    endfunction

    " lsp with haskell?
    lua << EOF
    local nvim_lsp = require('lspconfig')

    -- Use an on_attach function to only map the following keys
    -- after the language server attaches to the current buffer
    local on_attach = function(client, bufnr)
      local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
      local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

      -- Enable completion triggered by <c-x><c-o>
      buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

      -- Mappings.
      local opts = { noremap=true, silent=true }

      -- See `:help vim.lsp.*` for documentation on any of the below functions
      buf_set_keymap('n', 'gD', '<cmd>lua vim.lsp.buf.declaration()<CR>', opts)
      buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
      buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
      buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
      buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
      buf_set_keymap('n', ',wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
      buf_set_keymap('n', ',wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
      buf_set_keymap('n', ',wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
      buf_set_keymap('n', ',D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
      buf_set_keymap('n', ',rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
      buf_set_keymap('n', ',ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
      buf_set_keymap('n', ',cd', '<cmd>lua vim.lsp.codelens.run()<CR><cmd>lua vim.lsp.codelens.refresh()<CR>', opts)
      buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
      buf_set_keymap('n', ',e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
      buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
      buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
      buf_set_keymap('n', ',q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
      buf_set_keymap('n', ',f', '<cmd>lua vim.lsp.buf.formatting()<CR>', opts)

    end

    -- actually enable hls (haskell)
    require'lspconfig'.hls.setup{
      on_attach = on_attach,
      settings = {
        haskell = {
          formattingProvider = "brittany"
        }
      },
      capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities()),
    }

    -- enable elixirls (elixir)
    require'lspconfig'.elixirls.setup{
      on_attach = on_attach,
      cmd = { "elixir-ls" }
    }
    EOF

    " refresh code lenses on write
    autocmd BufWritePost * lua vim.lsp.codelens.refresh()

    " completion
    lua <<EOF
    local cmp = require'cmp'

    cmp.setup({
      snippet = {
        expand = function(args)
        end,
      },
      mapping = {
        ['<C-d>'] = cmp.mapping.scroll_docs(-4),
        ['<C-f>'] = cmp.mapping.scroll_docs(4),
        ['<C-Space>'] = cmp.mapping.complete(),
        ['<C-e>'] = cmp.mapping.close(),
      },
      preselect = cmp.PreselectMode.None,
      completion = {
        completeOpt = "noinsert,menuone,noselect",
      },
      sources = {
        { name = 'nvim_lsp' },
        { name = 'treesitter' },
        { name = 'buffer' },
        { name = 'path' },
      }
    })
    EOF

    " treesitter
    lua <<EOF
    require'nvim-treesitter.configs'.setup {
      ensure_installed = "maintained",
      -- sad, but the performance is abysmal on anything except the smallest files
      disable = { "haskell" },
      highlight = {
        enable = true,
      }
    }
    EOF

    " haskell syntax highlighting rules
    let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
    let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
    let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
    let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
    let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
    let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
    let g:haskell_backpack = 1   
    let g:haskell_classic_highlighting = 1   
    " haskell indentation rules
    let g:haskell_indent_if = 2
    let g:haskell_indent_let = 2
    let g:haskell_indent_case_alternative = 1

    command! Ghcid call fzf#run(fzf#wrap(
      \ {'source': 'cat .ghcid-output | rg --line-number "^\S+:(\d+|\(\d+,\d+\)-\(\d+,\d+\)):" | sed -E "s/\(([[:digit:]]+),([[:digit:]]+)\)-(.*)/\1:\2/" | awk -F: '''{printf "%s:%s:%s:%s:\n",$1,$2,$3,$4}''' | sed p | sed 1d | paste -d"\0" - - | awk -F: '''{printf "%s:%s:%s:%s:%s\n",$1,$5,$2,$3,$4}'''',
      \  'options': '-d: --with-nth="3.." --nth="3.." --preview="echo {} | awk -F: '''{printf \"%s %s\",\$1,\$2}''' | xargs -n2 print-range .ghcid-output"',
      \  'sink': function('s:cleanopen') }))

  '';
}
