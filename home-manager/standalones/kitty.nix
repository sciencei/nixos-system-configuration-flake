{ pkgs, theme }:
''
font_family ${theme.font.name}
font_size ${(toString theme.font.size)}

foreground ${theme.colors.foreground}
background ${theme.colors.background}

cursor ${theme.colors.foreground}

cursor_blink_interval 0

color0 ${theme.colors.color0}
color1 ${theme.colors.color1}
color2 ${theme.colors.color2}
color3 ${theme.colors.color3}
color4 ${theme.colors.color4}
color5 ${theme.colors.color5}
color6 ${theme.colors.color6}
color7 ${theme.colors.color7}
color8 ${theme.colors.color8}
color9 ${theme.colors.color9}
color10 ${theme.colors.color10}
color11 ${theme.colors.color11}
color12 ${theme.colors.color12}
color13 ${theme.colors.color13}
color14 ${theme.colors.color14}
color15 ${theme.colors.color15}
''

