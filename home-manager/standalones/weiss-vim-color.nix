{ pkgs }:
let
  fileSource = pkgs.writeTextFile {
  name = "weiss-vim-color";
  destination = "/colors/weiss.vim";
  text = ''
    hi clear syntax reset
    let g:colors_name = "weiss"
    if &background == "light"
        hi Boolean gui=NONE guifg=#9aaaff guibg=NONE
        hi ColorColumn gui=NONE guifg=NONE guibg=#f5f5f5
        hi Comment gui=NONE guifg=#969696 guibg=NONE
        hi Conceal gui=NONE guifg=#9aaaff guibg=NONE
        hi Conditional gui=NONE guifg=#4a4a4a guibg=NONE
        hi Constant gui=NONE guifg=#9aaaff guibg=NONE
        hi Cursor gui=reverse guifg=NONE guibg=NONE
        hi CursorColumn gui=NONE guifg=NONE guibg=#f5f5f5
        hi CursorLine gui=NONE guifg=NONE guibg=#f5f5f5
        hi CursorLineNr gui=NONE guifg=#969696 guibg=NONE
        hi DiffAdd gui=NONE guifg=NONE guibg=#f0fff0
        hi DiffChange gui=NONE guifg=NONE guibg=#f5f5f5
        hi DiffDelete gui=NONE guifg=NONE guibg=#fff0f0
        hi DiffText gui=NONE guifg=NONE guibg=#e3e3e3
        hi Directory gui=NONE guifg=#4a4a4a guibg=NONE
        hi Error gui=NONE guifg=NONE guibg=#fff0f0
        hi ErrorMsg gui=NONE guifg=NONE guibg=#fff0f0
        hi FoldColumn gui=NONE guifg=#c2c2c2 guibg=NONE
        hi Folded gui=NONE guifg=#969696 guibg=NONE
        hi Ignore gui=NONE guifg=NONE guibg=NONE
        hi IncSearch gui=NONE guifg=NONE guibg=#e3e3e3
        hi LineNr gui=NONE guifg=#c2c2c2 guibg=NONE
        hi MatchParen gui=NONE guifg=NONE guibg=#e3e3e3
        hi ModeMsg gui=NONE guifg=NONE guibg=NONE
        hi MoreMsg gui=NONE guifg=NONE guibg=NONE
        hi NonText gui=NONE guifg=#c2c2c2 guibg=NONE
        hi Normal gui=NONE guifg=#000000 guibg=#fefeff
        hi Number gui=NONE guifg=#9aaaff guibg=NONE
        hi Pmenu gui=NONE guifg=NONE guibg=#f5f5f5
        hi PmenuSbar gui=NONE guifg=NONE guibg=#ededed
        hi PmenuSel gui=NONE guifg=NONE guibg=#e3e3e3
        hi PmenuThumb gui=NONE guifg=NONE guibg=#dbdbdb
        hi Question gui=NONE guifg=NONE guibg=NONE
        hi Search gui=NONE guifg=NONE guibg=#ededed
        hi SignColumn gui=NONE guifg=#c2c2c2 guibg=NONE
        hi Special gui=NONE guifg=#9aaaff guibg=NONE
        hi SpecialKey gui=NONE guifg=#c2c2c2 guibg=NONE
        hi SpellBad gui=undercurl guisp=NONE guifg=NONE guibg=#fff0f0
        hi SpellCap gui=undercurl guisp=NONE guifg=NONE guibg=NONE
        hi SpellLocal gui=undercurl guisp=NONE guifg=NONE guibg=#f0fff0
        hi SpellRare gui=undercurl guisp=NONE guifg=NONE guibg=#ededed
        hi Statement gui=NONE guifg=#4a4a4a guibg=NONE
        hi StatusLine gui=NONE guifg=#262626 guibg=#ededed
        hi StatusLineNC gui=NONE guifg=#969696 guibg=#ededed
        hi StorageClass gui=NONE guifg=#4a4a4a guibg=NONE
        hi String gui=NONE guifg=#9aaaff guibg=NONE
        hi TabLine gui=NONE guifg=#969696 guibg=#ededed
        hi TabLineFill gui=NONE guifg=NONE guibg=#ededed
        hi TabLineSel gui=NONE guifg=#262626 guibg=#ededed
        hi Title gui=NONE guifg=#9aaaff guibg=NONE
        hi Todo gui=standout guifg=NONE guibg=NONE
        hi Type gui=NONE guifg=#4a4a4a guibg=NONE
        hi Underlined gui=NONE guifg=NONE guibg=NONE
        hi VertSplit gui=NONE guifg=#e3e3e3 guibg=NONE
        hi Visual gui=NONE guifg=NONE guibg=#e3e3e3
        hi VisualNOS gui=NONE guifg=NONE guibg=NONE
        hi WarningMsg gui=NONE guifg=NONE guibg=#fff0f0
        hi WildMenu gui=NONE guifg=NONE guibg=#d1d1d1
        hi lCursor gui=NONE guifg=NONE guibg=NONE
        hi Identifier gui=NONE guifg=NONE guibg=NONE
        hi PreProc gui=NONE guifg=NONE guibg=NONE
    elseif &background == "dark"
        hi Boolean gui=NONE guifg=#cbcbcd guibg=NONE
        hi ColorColumn gui=NONE guifg=NONE guibg=#b9d3e0
        hi Comment gui=NONE guifg=#e3edf2 guibg=NONE
        hi Conceal gui=NONE guifg=#cbcbcd guibg=NONE
        hi Conditional gui=bold guifg=#66ccff guibg=NONE
        hi Constant gui=NONE guifg=#cbcbcd guibg=NONE
        hi Cursor gui=reverse guifg=#434347 guibg=#fefeff
        hi CursorColumn gui=NONE guifg=#434347 guibg=#b9d3e0
        hi CursorLine gui=NONE guifg=#434347 guibg=#b9d3e0
        hi CursorLineNr gui=NONE guifg=#9aaaff guibg=NONE
        hi DiffAdd gui=NONE guifg=#434347 guibg=#66ccff
        hi DiffChange gui=NONE guifg=#434347 guibg=#faff9a
        hi DiffDelete gui=NONE guifg=#fefeff guibg=#ff6969
        hi DiffText gui=NONE guifg=#434347 guibg=#9affa1
        hi Directory gui=NONE guifg=#cbcbcd guibg=NONE
        hi Error gui=NONE guifg=#fefeff guibg=#ff6969
        hi ErrorMsg gui=NONE guifg=#fefeff guibg=#ff6969
        hi FoldColumn gui=NONE guifg=#b9d3e0 guibg=NONE
        hi Folded gui=NONE guifg=#cbcbcd guibg=NONE
        hi Ignore gui=NONE guifg=NONE guibg=NONE
        hi IncSearch gui=bold guifg=NONE guibg=#b9d3e0
        hi LineNr gui=NONE guifg=#b9d3e0 guibg=NONE
        hi MatchParen gui=NONE guifg=NONE guibg=#b9d3e0
        hi ModeMsg gui=NONE guifg=NONE guibg=NONE
        hi MoreMsg gui=NONE guifg=NONE guibg=NONE
        hi NonText gui=NONE guifg=#5e5e64 guibg=NONE
        hi Normal gui=NONE guifg=#fefeff guibg=#434347
        hi Number gui=NONE guifg=#cbcbcd guibg=NONE
        hi Pmenu gui=NONE guifg=NONE guibg=#899194
        hi PmenuSbar gui=NONE guifg=NONE guibg=#899194
        hi PmenuSel gui=NONE guifg=NONE guibg=#b9d3e0
        hi PmenuThumb gui=NONE guifg=NONE guibg=#cbcbcd
        hi Question gui=NONE guifg=NONE guibg=NONE
        hi Search gui=bold guifg=#fefefe guibg=#b9d3e0
        hi SignColumn gui=NONE guifg=#b9d3e0 guibg=NONE
        hi Special gui=NONE guifg=#fc66ff guibg=NONE
        hi SpecialKey gui=NONE guifg=#5e5e64 guibg=NONE
        hi SpellBad gui=NONE guifg=NONE guibg=#ff6969
        hi SpellCap gui=undercurl guisp=NONE guifg=NONE guibg=NONE
        hi SpellLocal gui=undercurl guisp=NONE guifg=#fefeff guibg=NONE
        hi SpellRare gui=undercurl guisp=NONE guifg=#fefeff guibg=NONE
        hi Statement gui=bold guifg=#66ccff guibg=NONE
        hi StatusLine gui=NONE guifg=#fefeff guibg=#899194
        hi StatusLineNC gui=NONE guifg=#fefeff guibg=#899194
        hi StorageClass gui=bold guifg=#b9d3e0 guibg=NONE
        hi String gui=NONE guifg=#ff6969 guibg=NONE
        hi TabLine gui=NONE guifg=#fefeff guibg=#899194
        hi TabLineFill gui=NONE guifg=#fefeff guibg=#899194
        hi TabLineSel gui=NONE guifg=#fefeff guibg=#b9d3e0
        hi Title gui=NONE guifg=#e3edf2 guibg=NONE
        hi Todo gui=standout guifg=#434347 guibg=#faff9a
        hi Type gui=NONE guifg=#80b9d6 guibg=NONE
        hi Underlined gui=NONE guifg=NONE guibg=NONE
        hi VertSplit gui=NONE guifg=#5e5e64 guibg=NONE
        hi Visual gui=NONE guifg=#434347 guibg=#b9d3e0
        hi VisualNOS gui=NONE guifg=#434347 guibg=NONE
        hi WarningMsg gui=NONE guifg=#434347 guibg=#faff9a
        hi WildMenu gui=NONE guifg=#fefeff guibg=#b9d3e0
        hi lCursor gui=NONE guifg=NONE guibg=NONE
        hi Identifier gui=NONE guifg=#9addff guibg=NONE
        hi PreProc gui=bold guifg=#9aaaff guibg=NONE
    endif
    '';
    };
in
  pkgs.vimUtils.buildVimPlugin {
    name = "weiss-vim-color";
    src = fileSource;
  }
