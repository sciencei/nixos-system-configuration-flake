let
 secret = "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGIlwua03YXknSobWXvPPfM9Doa563/4PITmNjZ8HsoL";
in
{
  "ssh.age".publicKeys = [secret];
}
