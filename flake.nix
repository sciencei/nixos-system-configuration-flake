{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    zsh-nix-shell = {
      url = "github:chisui/zsh-nix-shell";
      flake = false;
    };
    nur.url = "github:nix-community/NUR/master";
    neovim-nightly-overlay = {
      url = "github:nix-community/neovim-nightly-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.neovim-nightly-flake.inputs.nixpkgs.follows = "nixpkgs";
    };
    theme.url = "gitlab:sciencei/nixos-theme";
    agenix = {
      url = "github:ryantm/agenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    xmonad-config = {
      url = "gitlab:sciencei/xmonad-config";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, theme, agenix, home-manager, nur, neovim-nightly-overlay, zsh-nix-shell, xmonad-config }: {

    nixosConfigurations.sci-li-lap = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      modules =
        [
          agenix.nixosModules.age {
            age = {
              identityPaths = [ "/etc/ssh/id_nix" ];
              secrets.ssh = {
                file = ./secrets/ssh.age;
                path = "/home/sciencei/.ssh/config";
                owner = "sciencei";
              };
            };
          }
          ({ pkgs, config, lib, ... }: {
            
            imports = [
              ./hardware-configuration.nix
              home-manager.nixosModules.home-manager {
                home-manager = {
                  # why is one of these pkgs and one is packages
                  useGlobalPkgs = true;
                  useUserPackages = true;
                  users.sciencei =
                    import ./home-manager/home.nix
                      { inherit pkgs config lib theme nur neovim-nightly-overlay zsh-nix-shell xmonad-config; };
                };
              }
            ];

            # Let 'nixos-version --json' know about the Git revision
            # of this flake.
            system.configurationRevision = nixpkgs.lib.mkIf (self ? rev) self.rev;
            
            # nix settings
            nix = {
              # enable flakes
              extraOptions = ''
                experimental-features = nix-command flakes
              '';
              # iohk cache, for haskell development
              settings = {
                substituters = [
                  "https://hydra.iohk.io"
                ];
                trusted-public-keys = [
                  "hydra.iohk.io:f/Ea+s+dFdN+3Y/G+FDgSq+a5NEWhJGzdjvKNGv0/EQ="
                ];
              };
              # pin nixpkgs
              registry.nixpkgs.flake = nixpkgs;
            };
            nixpkgs = {
              # any overlays
              overlays = [
                ( self: super: {
                  ephemeralpg = super.ephemeralpg.override { postgresql = self.postgresql_12; };
                  # iirc, something gets set by default that conflicts with me installing
                  # extensions via home-manager. this works around that
                  firefox = super.wrapFirefox super.firefox-unwrapped {
                    extraPolicies = {
                      ExtensionSettings = {};
                    };
                  };
                })
                neovim-nightly-overlay.overlay
                nur.overlay
              ];
              config.allowUnfree = true;
            };

            # boot and kernel settings
            boot = {
              # use grub
              loader = {
                grub = {
                  enable = true;
                  efiSupport = true;
                  # where to install grub; this means nowhere
                  device = "nodev";
                  copyKernels = true;
                };
                efi.canTouchEfiVariables = true;
              };

              # unlock with luks
              initrd.luks.devices = {
                root = {
                  device = "/dev/disk/by-id/nvme-eui.5cd2e4299140b9dd-part2";
                  preLVM = true;
                  allowDiscards = true;
                };
              };

              # kernel
              kernelPackages = pkgs.linuxPackages_testing;
              kernel.sysctl = { "vm.swappiness" = 5; };
              initrd.verbose = false;
              kernelParams = [ "quiet" "udev.log_priority=3" ];

              # boot animation
              # TODO: add an actually custom one
              plymouth.enable = true;
            };

            # firmware stuff
            hardware = {
              firmware = with pkgs; [
                # to make sure sound open firmware works
                alsa-firmware
                sof-firmware
              ];

              cpu.intel.updateMicrocode = true;
            };
            # update firmware
            services.fwupd.enable = true;

            # power management
            powerManagement = {
              enable = true;
              powertop.enable = true;
            };
            services.logind = {
              lidSwitch = "suspend-then-hibernate";
              lidSwitchDocked = "ignore";
              lidSwitchExternalPower = "hybrid-sleep";
            };
            environment.etc."systemd/sleep.conf".text = ''
              [Sleep]
              HibernateDelaySec=15min
            '';
            ## tlp
            ## and no tlp-recommended kernel module for now because it was failing to load
            services.tlp = {
              enable = true;
              settings = {
                cpu_scaling_governor_on_ac = "performance";
                cpu_scaling_governor_on_bat = "powersave";
                cpu_hwp_on_ac = "performance";
                cpu_hwp_on_bat = "power";
              };
            };
            ## hibernate on power <= 5%
            services.udev.packages = [
              (pkgs.writeTextFile {
                name = "udev-low-power-hibernate";
                destination = "/etc/udev/rules.d/98-hibernate-on-low-power.rules";
                text = ''
                  subsystem=="power_supply", attr{status}=="discharging", attr{capacity}=="[0-5]", run+="${pkgs.udev}/bin/systemctl hibernate"
                '';
              })
            ];

            # networking
            networking = {
              # system information 
              hostName = "sci-li-lap";
              # enable networkmanager
              networkmanager.enable = true;
              # works around a mullvad thing
              iproute2.enable = true;
              firewall = {
                # works around a mullvad thing
                checkReversePath = "loose";

                allowedTCPPorts = [];
                allowedUDPPorts = [];
              };
              # The global useDHCP flag is deprecated, therefore explicitly set to false here.
              # Per-interface useDHCP will be mandatory in the future, so this generated config
              # replicates the default behaviour.
              useDHCP = false;
              interfaces.enp0s31f6.useDHCP = true;
              interfaces.wlp0s20f3.useDHCP = true;
            };
            ## local network discovery
            services.avahi.enable = true;
            ## vpn
            services.mullvad-vpn.enable = true;
            ## bluetooth
            hardware.bluetooth = {
              enable = true;
              package = pkgs.bluezFull;
              # bluetooth HSP/HFP
              hsphfpd.enable = true;
            };

            # thunderbolt
            services.hardware.bolt.enable = true;

            # sound
            sound.enable = true;
            services.pipewire = {
              enable = true;
              alsa = {
                enable = true;
                support32Bit = true;
              };
              pulse.enable = true;
            };
            
            # backlight
            programs.light.enable = true;
            services.actkbd = {
              enable = true;
              bindings = [
                { keys = [ 224 ]; events = [ "key" ]; command = "${pkgs.light}/bin/light -U 8"; }
                { keys = [ 225 ]; events = [ "key" ]; command = "${pkgs.light}/bin/light -A 8"; }
              ];
            };

            # graphics
            hardware.opengl = {
              enable = true;
              driSupport = true;
              extraPackages = with pkgs; [
                vaapiIntel
                intel-media-driver
              ];
              extraPackages32 = with pkgs.pkgsi686Linux; [ vaapiIntel ];
              setLdLibraryPath = true;
            };
            ## compositor
            services.picom = {
              enable = true;
              backend = "glx";
              fade = false;
              # no transparency on inactive windows
              inactiveOpacity = 1.0;
              shadow = false;
              vSync = true;
              experimentalBackends = true;
              settings = {
                "unredit-if-possible" = false;
              };
            };
            ## x11
            services.xserver = {
              enable = true;
              layout = "us";
              videoDrivers = [ "modesetting" ];

              # touchpad support
              libinput = {
                enable = true;
                touchpad.naturalScrolling = true;
              };

              displayManager = {
                
                # lightdm for login
                lightdm = {
                  enable = true;
                };

                # home-manager for session
                session = [
                  {
                    manage = "desktop";
                    name = "home-manager";
                    start = ''
                      ${pkgs.runtimeShell} $HOME/.hm-xsession &
                      waitPID=$!
                    '';
                  }
                ];
              };
            };

            # screen locking
            services.physlock = {
              enable = true;
              allowAnyUser = true;
              lockMessage = "Welcome! Please log in.";
              lockOn = {
                suspend = true;
                hibernate = true;
                extraTargets = [
                  "hybrid-sleep.target"
                  "suspend-then-hibernate.target"
                ];
              };
            };

            # printing
            services.printing = {
              enable = true;
              drivers = with pkgs; [
                gutenprint
                gutenprintBin
                hplip
              ];
            };

            # general environment stuff
            time.timeZone = "America/New_York";
            i18n.defaultLocale = "en_US.UTF-8";
            environment.shells = with pkgs; [ zsh ];
            ## fonts
            fonts.fonts = with pkgs; [
              corefonts
              # noto; good for general fallback
              google-fonts
              (theme.font.pkg pkgs)
            ];
            console = {
              font = "${theme.font.name}${toString(theme.font.size - 2)}";
              keyMap = "us";
              colors = map (x: builtins.substring 1 (builtins.stringLength x) x) [
                theme.colors.color0
                theme.colors.color1
                theme.colors.color2
                theme.colors.color3
                theme.colors.color4
                theme.colors.color5
                theme.colors.color6
                theme.colors.color7
                theme.colors.color8
                theme.colors.color9
                theme.colors.color10
                theme.colors.color11
                theme.colors.color12
                theme.colors.color13
                theme.colors.color14
                theme.colors.color15
              ];
            };

            # system programs
            environment.systemPackages = with pkgs; [
              # audio
              alsaLib
              alsaUtils
              ## for pactl
              pulseaudio

              # thunderbolt support
              bolt

              # bluetooth
              bluez

              # power management
              powertop
              tpacpi-bat

              # networking
              mullvad-vpn
              update-systemd-resolved
              wget

              # misc
              bash
              git
              pkgs.home-manager
              kitty
              libmtp
              neovim
              zsh
            ];

            # users
            users.users.sciencei = {
              isNormalUser = true;
              extraGroups = [ "wheel" "networkmanager" "audio" "adbusers" ];
              shell = pkgs.zsh;
            };

            # misc
            ## gnome keyring
            services.gnome.gnome-keyring.enable = true;
            security.pam.services.lightdm.enableGnomeKeyring = true;
            ## zsh autocomplete thing
            environment.pathsToLink = [ "/share/zsh" ];
            ## steam
            ## I feel slightly dirty putting this here rather than in home-manager but home-manager only
            ## lets me add it to my packages which isn't enough for it to actually work
            programs.steam.enable = true;
            ## allow connecting to kindle
            services.gvfs.enable = true;
            ## android
            programs.adb.enable = true;

          })
        ];
    };

  };
}
